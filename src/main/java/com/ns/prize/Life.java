package com.ns.prize;

import com.ns.Game;
import com.ns.Box;

public class Life extends Box {
    private Integer livesBonus;

    public Life(Integer livesBonus) {
        this.livesBonus = livesBonus;
    }

    public void applyResult(Game game) {
        super.applyResult(game);
        game.setLives(game.getLives() + livesBonus);
    }
}
