package com.ns;

public class Box {
    private boolean opened = false;
    public void applyResult (Game game) {
        if (opened) {
            throw new IllegalStateException("Can't get the same prize twice");
        }
        opened = true;
    }

    public boolean isOpened() {
        return opened;
    }

    public void setOpened(boolean opened) {
        this.opened = opened;
    }
}
