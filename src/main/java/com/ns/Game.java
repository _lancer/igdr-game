package com.ns;

import com.ns.prize.Life;
import com.ns.prize.MoneyBox;
import com.ns.prize.SecondChance;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Game {

    private boolean secondChanceUsed;
    private Integer totalMoney = 0;
    private Integer lives = 1;

    private List <Integer> availableBoxes;
    private List<Box> boxes = generateBoxes ();

    public void open (int selected) {
        if (lives == 0)
            throw new IllegalStateException("This game ended. You can not open boxes anymore");

        boxes.get(selected).applyResult(this);
        availableBoxes.remove(Integer.valueOf(selected));

        if (lives == 0)
            gameOver ();
    }


    public void gameOver () {

        List<Box> boxes = new ArrayList<>();

        boxes.add (new MoneyBox(20));
        boxes.add (new MoneyBox(10));
        boxes.add (new MoneyBox(5));
        if (!secondChanceUsed)
            boxes.add (new SecondChance());

        Collections.shuffle(boxes);

        boxes.get(0).applyResult(this);
    }

    public List<Box> generateBoxes () {
        List<Box> boxes = new ArrayList<>();

        boxes.add (new MoneyBox(100));
        boxes.addAll(IntStream.range(0, 2)
                .mapToObj(index -> new MoneyBox(20))
                .collect(Collectors.toList()));
        boxes.addAll(IntStream.range(0, 5)
                .mapToObj(index -> new MoneyBox(5))
                .collect(Collectors.toList()));
        boxes.add (new Life(1));
        boxes.addAll (IntStream.range(0, 3)
                .mapToObj(index -> new Life(-1))
                .collect(Collectors.toList()));

        Collections.shuffle(boxes);

        availableBoxes = IntStream.range(0, boxes.size()).boxed().collect(Collectors.toList());

        return boxes;
    }


    public List<Integer> getAvailableBoxes (){
        return new ArrayList<>(availableBoxes);
    }

    public boolean isOver () {
        return lives <= 0;
    }
    public List<Box> getBoxes() {
        return boxes;
    }

    public void setBoxes(List<Box> boxes) {
        this.boxes = boxes;
    }

    public boolean isSecondChanceUsed() {
        return secondChanceUsed;
    }

    public void setSecondChanceUsed(boolean secondChanceUsed) {
        this.secondChanceUsed = secondChanceUsed;
    }

    public Integer getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(Integer totalMoney) {
        this.totalMoney = totalMoney;
    }

    public Integer getLives() {
        return lives;
    }

    public void setLives(Integer lives) {
        this.lives = lives;
    }
}
