package com.ns;

import org.junit.Test;

import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Emulator {

    public static final int THREADS_COUNT = 10;
    ExecutorService executor = Executors.newFixedThreadPool(THREADS_COUNT);

    public static class Task implements Callable<Integer> {
        int emulations;
        Task (int emulations) {
            this.emulations = emulations;
        }
        @Override
        public Integer call() throws Exception {
            int emulation = 0;
            int summ = 0;
            while (emulation < emulations) {
                emulation+=1;

                Game game = new Game();

                Random rand = new Random();

                while (!game.isOver()) {
                    List<Integer> availableBoxes = game.getAvailableBoxes();
                    int selected = availableBoxes.get(rand.nextInt(availableBoxes.size()));
                    game.open(selected);
                }
                // System.out.println ();
               // System.out.println (Thread.currentThread().getId() + " earned " + game.getTotalMoney()
                 //       + "\t" + (double) emulation * 100 / emulations + "%");
                summ += game.getTotalMoney();

            }
            return summ;
        }
    }
    @Test
    public void emulation () throws InterruptedException, ExecutionException {
        int emulationsTotal = 10000000;
        int summ = 0;
        List<Future<Integer>> futures = executor.invokeAll(
            IntStream.range(0, THREADS_COUNT)
                    .mapToObj(task -> new Task( emulationsTotal/ THREADS_COUNT))
                    .collect(Collectors.toList()));

        for (Future<Integer> future : futures) {
            summ += future.get();
        }
//10.000.000: 73.571902 73.571902 73.5368935 73.5535075 73.529159

        System.out.println((double) summ / emulationsTotal);
    }

    @Test
    public void permutations () {
        List<List<Integer>> permutations = Permutations.permute( new int[]{100, 20,20, 5,5,5,5,5, -1, -2,-2,-2} );

        double mainPart = 0;
        for (List<Integer> permutation : permutations) {
            int variantSum = 0;
            boolean variantBonusLife = false;
            for (Integer val : permutation) {
                if (val > 0)
                    variantSum += val;
                else if (val == -1)
                    variantBonusLife = true;
                else if (val == -2) {
                    if (variantBonusLife)
                        variantBonusLife = false;
                    else
                        break;
                }
            }
            mainPart += variantSum;
        }
        mainPart = mainPart / permutations.size();

        double bonusWithoutSecondChance = 5./4 + 10./4 + 20./4;
        double wSecChance = mainPart * 1./4;
        double bonusWithSecondChance = (5./3 + 10./3 + 20./3) * 1./4;;
//73.54166666666667
        System.out.println( mainPart + wSecChance + bonusWithoutSecondChance + bonusWithSecondChance);

    }

}
