package com.ns.prize;

import com.ns.Game;
import com.ns.Box;

public class SecondChance extends Box {
    public void applyResult(Game game) {
        super.applyResult(game);
        game.setLives(1);
        game.setBoxes(game.generateBoxes());
        game.setSecondChanceUsed(true);
    }
}
