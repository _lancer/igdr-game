package com.ns;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.fail;

public class GameTest {

    @Test
    public void initStateGameTest () {
        Game game = new Game();
        Assert.assertEquals("Expected 0 money on start ", new Integer(0), game.getTotalMoney());
        Assert.assertEquals("Expected 1 life on start ", new Integer(1), game.getLives());
        Assert.assertEquals("Expected 12 boxes on start ",
                new Integer(12), new Integer(game.getBoxes().size()));
        Assert.assertFalse("Expected second chance used flag eq false ", game.isSecondChanceUsed());
        Assert.assertFalse("Game is not over at start ", game.isOver());
    }

    @Test (expected = IllegalStateException.class)
    public void pickBoxTwice () {
        Game game = new Game();
        game.open(1);
        game.open(1);

        fail ("Seems like you can open same box twice");
    }

    @Test (expected = IllegalStateException.class)
    public void gameOver () {
        Game game = new Game();
        game.setLives(0);

        Assert.assertTrue("Game over expected", game.isOver());
        game.open(1);
        fail ("Seems like you can open boxes when game over");
    }

}