package com.ns.prize;

import com.ns.Game;
import com.ns.Box;

public class MoneyBox extends Box {
    private Integer amount;

    public MoneyBox(Integer amount) {
        this.amount = amount;
    }

    public void applyResult(Game game) {
        super.applyResult(game);
        game.setTotalMoney(game.getTotalMoney() + amount);
    }
}
