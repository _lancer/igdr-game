package com.ns;

import com.ns.prize.Life;
import com.ns.prize.MoneyBox;
import com.ns.prize.SecondChance;
import org.junit.Assert;
import org.junit.Test;

public class BoxTest {

    @Test
    public void lifeBoxTest () {
        Game game = new Game ();

        Box box = new Life(1);
        Integer expected = game.getLives() + 1;
        box.applyResult(game);
        Assert.assertEquals("Life box does not change lives count ", expected, game.getLives());

        box = new Life(-1);
        expected = game.getLives() - 1;
        box.applyResult(game);
        Assert.assertEquals("Life box does not change lives count when value is negative", expected, game.getLives());
    }

    @Test
    public void moneyBoxTest () {
        Game game = new Game ();

        Box box = new MoneyBox(100);
        Integer expected = game.getTotalMoney() + 100;
        box.applyResult(game);
        Assert.assertEquals("Money box does not change money amount ", expected, game.getTotalMoney());

    }

    @Test
    public void secondChanceTest () {
        Game game = new Game ();


        Integer expectedLives = game.getLives();
        // given monay amount changed
        game.setTotalMoney( 100 );
        Integer expectedMoney = game.getTotalMoney();

        Box box = new SecondChance();
        box.applyResult(game);

        Assert.assertEquals("Expected same amout of money after second chance ", expectedMoney, game.getTotalMoney());
        Assert.assertEquals("Expected same life count like on start ", expectedLives, game.getLives());
        Assert.assertTrue("Expected second chance used flag eq true ", game.isSecondChanceUsed());

    }
}